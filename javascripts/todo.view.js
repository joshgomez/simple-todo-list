
var TodoView = Backbone.View.extend({

	events: {
	   "submit form" 		: 	"handleAddThing",
	   "click button" 		: 	"handleDeleteThing",
	   "click .sort a" 		: 	"handleSortThing", 
	   "click .done" 		: 	"handleDoneThing"
	},
	 
	initialize: function(){
	
		this.templateFiles = [
			'templates/submit-add.html',
			'templates/todo-body.html',
			'templates/todo-item.html',
			'templates/todo-item-empty.html'
		];
		
		this.templates = {};
		
		var self = this;
		
		$.each(this.templateFiles,function(index,tpl){
		
			$.get(tpl, function(html){
			
				self.templates[tpl] = html;
				self._updateLoadedTemplates();
			})
		
		});
		
		$(document).on("loadedTemplates", function(e){
		
			if(_.size(self.templates) == _.size(self.templateFiles))
			{
				self._preRender();
				self.render();
				
				self.collection.on('add', function(){
				
					this.renderMessage('Added thing todo in the list!');
					this.render();
					
				},self);
				
				self.collection.on('remove sort', self.render,self);
				self.collection.on("invalid", self.renderError,self);
			}
		
		});
	},
	
	_updateLoadedTemplates:function(html)
	{
	
		$.event.trigger({
		
			type 			: "loadedTemplates",
			viewContext 	: this
		});
	
	},
	
	_preRender:function()
	{
	
		var template 	= 	_.template( this.templates['templates/submit-add.html'], {});
		template 		+= 	_.template( this.templates['templates/todo-body.html'], {});
		this.$el.html( template );
	
	},
	
	renderError:function(models,error)
	{
		var $elSubmitMessage = this.$el.find('form+p');
		$elSubmitMessage.html(error);
	},
	
	renderMessage:function(msg)
	{
		var $elSubmitMessage = this.$el.find('form+p');
		$elSubmitMessage.html(msg);
	},
	
	render: function(){

		var htmlItems = "";
		
		var self = this;
		$.each(this.collection.models,function(index,model){
			
			var checked = (model.attributes.done == 1) ? ' checked' : '';
			
			var variables = { 
			
				todo: _.escape(model.attributes.todo),
				checked: checked,
				index: index
				
			};
			
			htmlItems += _.template( self.templates['templates/todo-item.html'],variables);
		});
		
		if(!htmlItems)
			htmlItems = _.template( this.templates['templates/todo-item-empty.html'],{});
		
		this.$el.find('ul').html( htmlItems );
		
	},
	
	//Events
	
	handleAddThing: function(e){
	 
		e.preventDefault();
		var form = e.currentTarget;

		this.collection.addThing(
			form.todo.value
		);
		
		form.reset();
		
	},
	
	handleDeleteThing: function(e){

		e.preventDefault();
		this.collection.deleteThing(e.target.value)
	},
	
	handleDoneThing: function(e){

		this.collection.doneThing(e.target.value)
	},
	
	handleSortThing: function(e){

		e.preventDefault();
		
		var $t = $(e.target);
		var sort = $t.attr('data-sort');
		if(sort)
		{
			var $th = this.$el.find('.sort > a');
			
			$th.removeClass("active");
			$th.removeClass("active");
			$t.addClass("active").toggleClass("up")

			this.collection.sortThing(sort, $th.hasClass("active up"));
		}
	}
});


