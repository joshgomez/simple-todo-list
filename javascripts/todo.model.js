
var Thing = Backbone.Model.extend({

	defaults:
	{
		done 	 	: 0
	},
	
	validate: function( attributes )
	{
		if( attributes.todo == 'undefined' || attributes.todo == '')
		{
			return "Please fill in the thing todo.";
		}
	}
});